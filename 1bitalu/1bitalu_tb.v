`timescale 1ns/1ps
module testbench;
  reg in1, in2, binv, cin;
  reg [0:1] op;
  wire cout, res;
  
  alu1 ALU1(.a(in1), .b(in2), .binv(binv), .cin(cin), .op(op), .cout(cout), .res(res));

  
  initial begin
   $monitor("A = %b B = %b BINV = %b CIN = %b COUT = %b RES = %b OP = %b", in1, in2, binv, cin, cout, res, op);

      in1 = 0; in2 = 0; binv = 0; cin = 0; op = 0;
      #1 in1 = 0; in2 = 0; binv = 0; cin = 1; op = 0;
      #1 in1 = 0; in2 = 1; binv = 0; cin = 0; op = 0;
      #1 in1 = 0; in2 = 1; binv = 0; cin = 1; op = 0;
      #1 in1 = 1; in2 = 0; binv = 0; cin = 0; op = 0;
      #1 in1 = 1; in2 = 0; binv = 0; cin = 1; op = 0;
      #1 in1 = 1; in2 = 1; binv = 0; cin = 0; op = 0;
      #1 in1 = 1; in2 = 1; binv = 0; cin = 1; op = 0;
      #1 in1 = 0; in2 = 0; binv = 0; cin = 0; op = 1;
      #1 in1 = 0; in2 = 0; binv = 0; cin = 1; op = 1;
      #1 in1 = 0; in2 = 1; binv = 0; cin = 0; op = 1;
      #1 in1 = 0; in2 = 1; binv = 0; cin = 1; op = 1;
      #1 in1 = 1; in2 = 0; binv = 0; cin = 0; op = 1;
      #1 in1 = 1; in2 = 0; binv = 0; cin = 1; op = 1;
      #1 in1 = 1; in2 = 1; binv = 0; cin = 0; op = 1;
      #1 in1 = 1; in2 = 1; binv = 0; cin = 1; op = 1;
      #1 in1 = 0; in2 = 0; binv = 0; cin = 0; op = 2;
      #1 in1 = 0; in2 = 0; binv = 0; cin = 1; op = 2;
      #1 in1 = 0; in2 = 1; binv = 0; cin = 0; op = 2;
      #1 in1 = 0; in2 = 1; binv = 0; cin = 1; op = 2;
      #1 in1 = 1; in2 = 0; binv = 0; cin = 0; op = 2;
      #1 in1 = 1; in2 = 0; binv = 0; cin = 1; op = 2;
      #1 in1 = 1; in2 = 1; binv = 0; cin = 0; op = 2;
      #1 in1 = 1; in2 = 1; binv = 0; cin = 1; op = 2;
      #1 in1 = 0; in2 = 0; binv = 1; cin = 0; op = 2;
      #1 in1 = 0; in2 = 0; binv = 1; cin = 1; op = 2;
      #1 in1 = 0; in2 = 1; binv = 1; cin = 0; op = 2;
      #1 in1 = 0; in2 = 1; binv = 1; cin = 1; op = 2;
      #1 in1 = 1; in2 = 0; binv = 1; cin = 0; op = 2;
      #1 in1 = 1; in2 = 0; binv = 1; cin = 1; op = 2;
      #1 in1 = 1; in2 = 1; binv = 1; cin = 0; op = 2;
      #1 in1 = 1; in2 = 1; binv = 1; cin = 1; op = 2;
      #1 in1 = 0; in2 = 0; binv = 0; cin = 0; op = 3;
      #1 in1 = 0; in2 = 0; binv = 0; cin = 1; op = 3;
      #1 in1 = 0; in2 = 1; binv = 0; cin = 0; op = 3;
      #1 in1 = 0; in2 = 1; binv = 0; cin = 1; op = 3;
      #1 in1 = 1; in2 = 0; binv = 0; cin = 0; op = 3;
      #1 in1 = 1; in2 = 0; binv = 0; cin = 1; op = 3;
      #1 in1 = 1; in2 = 1; binv = 0; cin = 0; op = 3;
      #1 in1 = 1; in2 = 1; binv = 0; cin = 1; op = 3;
  end

endmodule //1bitalutest
