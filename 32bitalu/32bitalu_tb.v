/*Stephen Zeng
32-bit ALU Testbench*/
`timescale 1ns/1ps
module testbench;
  reg [0:31] a, b;
  reg [0:2] op; //And = 0, Or = 1, FA = 2, Xor = 3 Last bit is only for subtraction
  wire [0:31] res;
  wire zf, cf, of, sf;
  
  alu32 ALU32(.a(a), .b(b), .op(op), .res(res), .zf(zf), .cf(cf), .of(of), .sf(sf));
  
  initial begin
   $monitor("A = %h B = %h OP = %h RES= %h ZF = %h CF = %h OF = %h SF = %h", a, b, op, res, zf, cf, of, sf);
   /* #0 a = 0; b = 0; op = 0; //Add
    #1 a = 'hf; b = 'hf; op = 0;
    #1 a = 0; b = 'hf; op = 0;
    #1 a = 'hffffffff; b = 1; op = 0;
    #1 a = 0; b = 0; op = 1; //Or
    #1 a = 'hf; b = 'hf; op = 1;
    #1 a = 0; b = 'hf; op = 1;
    #1 a = 'hffffffff; b = 1; op = 1;
    #1 a = 0; b = 0; op = 2; //Xor
    #5 a = 'hf; b = 'hf; op = 2;
    #5 a = 0; b = 'hf; op = 2;
    #5 a = 'hffffffff; b = 1; op = 2;*/
   // #5 a = 0; b = 0; op = 3; //Add
    #1 a = 'hf; b = 'hf; op = 3;
   /* #5 a = 0; b = 'hf; op = 3;
    #5 a = 'hffffffff; b = 1; op = 3;
    #5 a = 0; b = 0; op = 7; //Sub
    #5 a = 'hf; b = 'hf; op = 7;
    #5 a = 0; b = 'hf; op = 7;
    #5 a = 'hffffffff; b = 1; op = 7;*/
  end

endmodule // 32 bit alu testbench
