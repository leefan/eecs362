`timescale 1ns/1ps
module mux21(in0, in1, sel, out);
  input in0, in1, sel;
  output out;

  assign a0 = in0 & ~sel;
  assign b0 = in1 & sel;
  assign out = a0 | b0;
endmodule //mux21
