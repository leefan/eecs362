# Module headers and instructions

module top_alu(
  input [0:31] ra, rb,
  input [0:15] imm16,
  input setinstr, imminstr, shiftinstr,
  input [0:2] aluop, setop
  input shiftarith, shiftdir,
  output [0:31] rd);

module instrfetch(
  input clk,
  PC_reset,
  branch;
  input [0:31] branch_val;
  output [0:31] instru;);

module decoder(
  input [0:31] instru;
  output [0:4] rs1, rs2, rd;  //output registers
  output [0:15] imm16;        //I-type
  output [0:2] aluop;		  //ALU Opcode
  output [0:25] name;		  //J-type
  output imminstr, immext;	  //immediate instruction, immediate extension (unsigned = 0, signed = 1)
  output zerocmp;			  //Tests EQ/NEQ (EQ = 1, NEQ = 0);
  output regwrt, memwrt, memtreg; //register-memory signals
);

module regfile(
  input clk, reset, write;
  input [0:4] wAddr,
  input [0:31] wData,
  input [0:4] rAddr1,
  output [0:31] rData1,
  input [0:4] rAddr2,
  output [0:31] rData2);

module alu32(
  input [0:31] a, b;
  input [0:2] op;
  output [0:31] res;
  output zf, cf, of, sf);

module shifter(
  input [0:31] in, offset,
  input arith, direction, //direction - 0 for right, 1 for left
  output [0:31] out);

        3'b000 : ;//SEQ Set On Equal
        3'b001 : ;//SNE Set On Not Equal
        3'b010 : ;//SLT Set On Less Than
        3'b011 : ;//SGT Set On Greater Than
        3'b100 : ;//SLE Set On Less Than Or Equal
        3'b101 : ;//SGE Set On Greater Than Or Equal


OPS
BEQZ 	Branch On Integer Equal to Zero
BNEZ 	Branch On Integer Not Equal to Zero
ADDI 	Integer Add Immediate (Signed)
ADDUI 	Integer Add Unsigned Immediate
SUBI 	Integer Subtract Immediate (Signed)
SUBUI 	Integer Subtract Unsigned Immediate
ANDI 	Logical And Immediate (Signed)
ORI		Logical Or Immediate (Signed)
XORI	Logical Xor Immediate
LHI		Load High Immediate
JR		Jump Register
JALR	Jump and Link Register
SLLI	Shift Left Logical Immediate
SRLI	Shift Right Logical Immediate
SRAI	Shift Right Arithmetic Immediate
SEQI	Set On Equal to Immediate
SNEI	Set Not Equal to Immediate
SLTI	Set On Less Than Immediate
SGTI	Set Greater Than Immediate
SLEI	Set Less Than Or Equal To Immediate
SGEI	Set Greater Than Or Equal To Immediate
LB		Load Byte
LH		Load Halfword
LW		Load Word
LBU		Load Byte Unsigned
LHU		Load Halfword Unsigned
SB		Store Byte
SH		Store Halfword
SW		Store Word
SLL		Shift Left Logical
SRL		Shift Right Logical
SRA		Shift Right Arithmetic
NOP
ADD		Integer Add (Signed)
ADDU	Integer Add Unsigned
SUB		Integer Subtract (Signed)
SUBU	Integer Subtract Unsigned
AND		Logical And
OR		Logical Or
XOR		Logical Xor
SEQ		Set On Equal
SNE		Set On Not Equal
SLT		Set On Less Than
SGT		Set On Greater Than
SLE		Set On Less Than Or Equal
SGE		Set On Greater Than Or Equal
MOVFP2I	Move SPFP To Integer
MOVI2FP	Move Integer To SPFP
J		Jump
JAL		Jump and Link

