/* Stephen Zeng 
Instruction Decoder Testbench*/
`timescale 1ns/1ps
module instrdecoder_tb;
  parameter IMEMFILE = "instr.hex"; 
  reg [8*80-1:0] filename;
  reg clk, reset, wrt, PC_src;
  reg [0:31] imm32;
  wire [0:31] instru;
  wire [0:31] PC_next;
  reg src;  
  wire [0:4] rs1, rs2, rd;
  wire [0:15] imm16;
  wire [0:2] aluop;
  wire [0:25] name;
  wire imminstr, immext;
  wire jumpinstr, rinstr;
  wire branchinstr, branchop;
  wire regwrt, memwrt, memtreg;
  wire set;
  wire [0:2] setop;
  wire shiftdir, shifttype;
  wire mult;
  wire [0:1] dsize;

  instrfetch INSTRFETCH(.clk(clk), .PC_reset(reset), .PC_src(src), .wrt(wrt), .branch_in(imm32), .instru(instru), .PC_next(PC_next));
  decoder DECODER(.instru(instru), .rs1(rs1), .rs2(rs2), .rd(rd), .imm16(imm16), .aluop(aluop), .name(name), .imminstr(imminstr), .sign(immext), .jumpinstr(jumpinstr), .rinstr(rinstr), .branchinstr(brancinstr), .branchop(branchop), .regwrt(regwrt), .memwrt(memwrt), .memtreg(memtreg), .dsize(dsize), .setinstr(set), .setop(setop), .shiftdir(shiftdir), .shiftarith(shifttype), .multinstr(mult));

  always
	#5 clk = !clk;
  initial begin
   if (!$value$plusargs("instrfile=%s", filename)) begin
            filename = IMEMFILE;
   end
    $readmemh(filename, INSTRFETCH.Instrmem.mem);
    $monitor("Opcode = %h \n IS = %b \n Time = %d \n PC Value = %h \n Instruction = %h \n RS1 = %d \n RS2 = %d \n RD = %d \n IMM16 = %h \n ALUOP = %b \n Name = %h \n Imminstr = %b \n Immext = %b \n Jumpinstr = %b \n Rinstr = %b \n Branchinstr = %b \n Branchop = %b \n Regwrite = %b \n Memwrt = %b \n Set = %b \n Setop = %b \n Shiftdir = %b \n Shifttype = %b \n \n", 
	     DECODER.opcode, DECODER.is, $time, PC_next, INSTRFETCH.instru, rs1, rs2, rd , imm16, aluop, name, imminstr, immext, jumpinstr, rinstr, branchop, regwrt, memwrt, memtreg, set, setop, shiftdir, shifttype);
      	clk = 0; reset = 1; wrt = 0; src = 0;
	#5 reset = 0; imm32 = 0; wrt = 1;
	#15 reset = 1; imm32 = 0;
    end
    initial 
      #100 $finish;

endmodule
