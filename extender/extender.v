/* Stephen Zeng
Extends immediates and jump values.
Used in I-type and J-type instructions*/
module extender(
	imm16,
	name,
	imminstr,
	immext,
	ext_val);
   
	//Input values
	input [0:15] imm16; //16-bit immediate
	input [0:25] name;  //Jump location
	input imminstr, immext; //Unsigned = 0, Signed = 1;
	output [0:31] ext_val;
	//Code Begin
	wire [0:31] imm_ext, jump_ext; //Extention value Temp input for full adder
       	//Code Start
	assign jump_ext[6:31] = name[0:25];
	genvar i;
	generate
	//Sign extend jump name value;   
	for (i = 0; i < 6; i = i + 1) begin : jumpext
	    assign jump_ext[i] = name[0]; //Distribute bits
	end
	//Extend imm16 value
        for (i = 0; i < 16; i = i +1) begin: imm16ext
            mux21 imm(.in0(1'b0), .in1(imm16[0]), .sel(immext), .out(imm_ext[i])); //Extend Unsigned/Signed
        end
        assign imm_ext[16:31] = imm16[0:15];
		//Selects the final extended value.
		for (i = 0; i < 32; i = i + 1) begin: extselloop 
			mux21 extsel(.in0(jump_ext[i]), .in1(imm_ext[i]), .sel(imminstr), .out(ext_val[i]));
		end
	endgenerate
   
endmodule //Extend unit
