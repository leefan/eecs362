/* Stephen Zeng
Extender Testbench*/
module extender_tb;
	reg [0:15] imm16;
	reg [0:25] name;
	reg imminstr, immext;
	wire [0:31] ext_val;

	extender EXTENDER(.imm16(imm16), .name(name), .imminstr(imminstr), .immext(immext), .ext_val(ext_val));

	initial begin
		$monitor("Imm16 = %h Name = %h Imminstr = %b Immext = %b Ext_val = %h", imm16, name, imminstr, immext, ext_val);
			#0 imm16 = 0; name = 0; imminstr = 0; immext = 0;
			#5 imm16 = 0; name = 1; imminstr = 0; immext = 0;
			#10 imm16 = 1; name = 'h2000000; imminstr = 0; immext = 0;
			#15 imm16 = 0; name = 'hfbcaad; imminstr = 0; immext = 1;
			#20 imm16 = 0; name = 'hb; imminstr = 1; immext = 0;
			#25 imm16 = 1; name = 0; imminstr = 1; immext = 0;
			#30 imm16 = 'h8000; name = 0; imminstr = 1; immext = 0;
			#35 imm16 = 'h8000; name = 0; imminstr = 1; immext = 1;
	end
endmodule //Extender Testbench

