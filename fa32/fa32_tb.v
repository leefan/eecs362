`timescale 1ns/1ps
module fa32_tb;
  reg [0:31] a, b;
  wire [0:31] sum, cout;
  
  fa32 FA32(.a(a), .b(b), .sum(sum), .cout(cout));
  
  initial begin
   $monitor("A = %h B = %h SUM = %h COUT= %h", a, b, sum, cout);
    #0 a = 0; b = 0;//Add
    #5 a = 'hf; b = 'hf;
    #5 a = 0; b = 'hf;
    #5 a = 'hffffffff; b = 1;
  end

endmodule // 32 bit full adder testbench
