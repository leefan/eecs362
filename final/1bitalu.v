`timescale 1ns/1ps
module alu1(a, b, binv, cin, op, cout, res);
    input a, b, binv, cin;
    input [0:1] op; //And = 0, Or = 1, FA = 2, Xor = 3 
    output cout, res;

    wire muxbi, fa_sum, muxop;
    mux21 a0 (b, ~b, binv, muxbi);
    fa b0 (a, muxbi, cin, fa_sum, cout);
    
    assign and_op = a & muxbi;
    assign or_op = a | muxbi;
    assign xor_op = a ^ muxbi;
    
    mux41 c0 (and_op, or_op, fa_sum, xor_op, op, res);

endmodule //alu1
