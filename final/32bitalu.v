/* Stephen Zeng 
32-bit ALU */
`timescale 1ns/1ps
module alu32(
  a, //32 bit A input
  b, // 32 bit B input
  op, // Operation Code
  res, // Result
  zf, // Zero Flag
  cf, // Carry Flag
  of, // Offset Flag
  sf); // Sign Flag
  //32 bit Inputs
  input [0:31] a, b;
  // Op Codes (And = 0, Or = 1, Xor = 2, FA = 3, Sub = 7)
  input [0:2] op;
  //32 bit result
  output [0:31] res;
  //Flag outputs
  output zf; 
  output cf; 
  output of; 
  output sf;
  //Operation results
  wire [0:31] and_op; //And result
  wire [0:31] or_op;  //Or result
  wire [0:31] xor_op; //Xor result
  wire [0:31] new_b;  //Updated B
  wire [0:31] fa_sum; //Full Adder result
  wire [0:31] cout; //Cout from Full Adder
  //Code Start
  assign and_op = a & b; //And operation
  assign or_op = a | b; //Or operation
  assign xor_op = a ^ b; //Xor operation
  genvar i; //Variable for loop
  generate //Start Loops
    for (i = 31; i >= 0; i = i - 1) begin : InvB //Inverses B for subtraction
      mux21 a0 (b[i], ~b[i], op[0], new_b[i]);
    end
    fa adder0 (a[31], new_b[31], op[0], fa_sum[31], cout[31]); //Full Adder Initialize
    for (i = 30; i >= 0; i = i - 1) begin : FullAdd //Full Adder
	   fa adder_i (a[i], new_b[i], cout[i+1], fa_sum[i], cout[i]);
    end   
    for (i = 31; i >= 0; i = i - 1) begin: Mux //Final result
      mux41 c0 (and_op[i], or_op[i], xor_op[i], fa_sum[i], op[1:2], res[i]);
    end   
  endgenerate
  assign zf = !res; //Correct Zero flag
  assign cf = cout[0]; //Extract carry flag
  assign of = cout[1] ^ cf; //Extract overflow flag
  assign sf = res[0]; //Sign Flag
  
endmodule //32bitalu
