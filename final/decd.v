module decd24(
    input [0:1] in,
    output [0:3] out
    );
    
    

    assign out[3] = ~in[0] & ~in[1];
    assign out[2] = ~in[0] & in[1];
    assign out[1] = in[0] & ~in[1];
    assign out[0] = in[0] & in[1];
endmodule

module decd38(
    input enable,
    input [0:2] in,
    output [0:7] out
    );

   

    assign out[7] = ~in[0] & ~in[1] & ~in[2] & enable;
    assign out[6] = ~in[0] & ~in[1] & in[2] & enable;
    assign out[5] = ~in[0] & in[1] & ~in[2] & enable;
    assign out[4] = ~in[0] & in[1] & in[2] & enable;
    assign out[3] = in[0] & ~in[1] & ~in[2] & enable;
    assign out[2] = in[0] & ~in[1] & in[2] & enable;
    assign out[1] = in[0] & in[1] & ~in[2] & enable;
    assign out[0] = in[0] & in[1] & in[2] & enable;
endmodule

module decd532(
    input [0:4] in,
    output [0:31] out
    );

    wire [0:3] control;
    decd24 DECODER2TO4(in[0:1], control);
    decd38 DECODER3TO8A(control[3], in[2:4], out[24:31]);
    decd38 DECODER3TO8B(control[2], in[2:4], out[16:23]);
    decd38 DECODER3TO8C(control[1], in[2:4], out[8:15]);
    decd38 DECODER3TO8D(control[0], in[2:4], out[0:7]);
endmodule

/*
module testbench;
    reg [0:4] in;
    wire [0:31] out;

    integer i;
    
    decd532 DECODER(in, out);

    initial begin
        $monitor("In = %d Out = %b", in, out);
        in = 0;
        for(i=0;i<31;i=i+1) begin
            #1 in = in+1;
        end
    end
endmodule
*/
