/* Stephen Zeng 
Instruction Decoder */
`timescale 1ns/1ps
module decoder(
  instru,
  rs1,
  rs2,
  rd,
  imm16,
  aluop,
  name,
  imminstr,
  sign,
  jumpinstr,
  rinstr,
  branchinstr,
  branchop,
  regwrt,
  memwrt,
  memtreg,
  dsize,
  setinstr,
  setop,
  shiftinstr,
  shiftdir,
  shiftarith,
  multinstr
  );
  //Input variable
  input [0:31] instru;
  //Output variables
  output reg [0:4] rs1, rs2, rd; //Output Registers
  output reg [0:15] imm16; //I-type
  output reg [0:2] aluop; //ALU Opcode
  output reg [0:25] name; // J-type
  output reg imminstr; //Immediate Instruction 
  output reg sign; // Unsigned = 0, Signed = 1
  output reg jumpinstr, rinstr; //J-type and R-type instructions
  output reg branchinstr; //Branching Instruction
  output reg branchop; //Tests EQ/NEQ (EQ = 1, NEQ =0)
  output reg regwrt, memwrt, memtreg;//Register-Memory Signals
  output reg [0:1] dsize;
  output reg setinstr; //Set bit
  output reg [0:2] setop;
  output reg shiftinstr; //Shift instruction
  output reg shiftdir; // 0 = Left, 1 = Right 
  output reg shiftarith; // 0 = Logical 1 = Arithmetic
  output reg multinstr; //Multiplication Instruction
  //Variables
  wire [0:5] opcode; 
  reg [0:5] func; // R-type
  wire isI0, isI1, isI2, isI3, isI4, isI5, isI6, isI7, isI, isR, isJ0, isJ1, isJ; //Instruction type
  wire [0:2] is;
  //Code Start
  assign opcode[0:5] = instru[0:5]; //Extract opcode
  //Determine if I-type
  assign isI0 = ~(opcode[1]) &  opcode[3] &  opcode[4];
  assign isI1 = ~(opcode[0]) & opcode[2] & ~(opcode[4]);
  assign isI2 = ~(opcode[1]) & ~(opcode[2]) & opcode[3];
  assign isI3 = ~(opcode[0]) & opcode[2] & ~(opcode[3]);
  assign isI4 = ~(opcode[0]) & opcode[1] & ~(opcode[2]) & opcode[4];
  assign isI5 = opcode[0] & ~(opcode[1]) & ~(opcode[3]) & ~(opcode[4]);
  assign isI6 = opcode[0] & ~(opcode[1]) & opcode[4] &  opcode[5];
  assign isI7 = ~(opcode[0]) & opcode[3] & ~(opcode[4]) & ~(opcode[4]);
  assign isI = isI0 | isI1 | isI2 | isI3 | isI4 | isI5 | isI6 | isI7;
  //Determine if R-type
  assign isR = !(opcode[0]) & !(opcode[1]) & !(opcode[2]) & !(opcode[3]) & !(opcode[4]);
  //Determine if J-type  
  assign isJ0 = !(opcode[0]) & !(opcode[1]) & !(opcode[2]) & !(opcode[3]) & opcode[4];
  assign isJ1 = !(opcode[0]) & opcode[1] & !(opcode[2]) & !(opcode[3]) & !(opcode[4]);
  assign isJ = isJ0 | isJ1;
  assign is[0:2] = {isI, isR, isJ};
  always @ (is or instru or opcode)
  case (is) //Instruction Type
  //I-type Instructions
    3'b100 : begin 
      rs1 = instru[6:10]; //Source Register
      rd[0:4] = instru[11:15]; //Destination Register
      imm16[0:15] = instru[16:31];
      name = 0;
      rs2 = 0;
      imminstr = 1;
      jumpinstr = 0;
      rinstr = 0;
       sign = 0;
       shiftinstr = 0;
       shiftarith = 0;	
       shiftdir = 0;
       shiftarith = 0;
       branchinstr = 0;
       branchop = 0;
       setinstr = 0;
       setop = 0;
       memwrt = 0;
       memtreg = 0;
       regwrt = 0;
       multinstr = 0;
      case(opcode[0:2]) //Opcode [0:2]
        3'b000 : begin
	    //TODO Stall PC
          case(opcode[3:5])
            3'b100 : begin//BEQZ Branch On Integer Equal To Zero
                       branchinstr = 1;
                       aluop = 7;
                       sign = 1;
                       branchop = 1;
                    end
            3'b101 : begin//BNEZ Branch On Integer Not Equal To Zero
    		       branchinstr = 1;
                       aluop = 7;
                       sign = 1;
                       branchop = 0;
                    end
          endcase
        end
        3'b001 : begin
           regwrt = 1;
          case(opcode[3:5])
            3'b000 : begin //ADDI Integer Add Immediate (Signed)
                       aluop = 3;
                       sign = 1;
                    end
            3'b001 : begin//ADDUI Integer Add Unsigned Immediate
                       aluop = 3;
                       sign = 0;
                    end
            3'b010 : begin//SUBI Integer Subtract Immediate (Signed)
                       aluop = 7;
                       sign = 1;
                    end
            3'b011 : begin//SUBUI Integer Subtract Unsigned Immediate
                       aluop = 7;
                       sign = 0;
                    end
            3'b100 : begin//ANDI Logical And Immediate (Signed)
                       aluop = 0;
                       sign = 1;
                    end
            3'b101 : begin//ORI Logical Or Immediate (Signed)
                       aluop = 1;
                       sign =1;
                    end
            3'b110 : begin//XORI Logical Xor Immediate
                       aluop = 2;
                       sign = 1;
                    end
            3'b111 : ;//LHI Load High Immediate
          endcase
	end
        3'b010 : begin
          case(opcode[3:5])
            3'b010 : begin//JR Jump Register
                   jumpinstr = 1;
                   rinstr = 1;
		end
            3'b011 : begin //JALR Jump And Link Register
                   rd = 'd31;
		           regwrt = 1;
                   jumpinstr = 1;
                   rinstr = 1;
                end
            3'b100 : begin //SLLI Shift Left Logical Immediate
		   shiftinstr = 1;
                   shiftdir = 0;
                   shiftarith = 0;
                end
            3'b110 : begin//SRLI Shift Right Logical Immediate (0's)
                   shiftinstr = 1;
		   shiftdir = 1;
                   shiftarith = 0;
                end
            3'b111 : begin//SRAI Shift Right Arithmetic Immediate (High bit)
                   shiftinstr = 1;
		   shiftdir = 1;
                   shiftarith = 1;
                end
          endcase
        end
        3'b011 : begin
           setinstr =1;
           setop = opcode[3:5];  
           regwrt = 1;       
          case(opcode[3:5])
            3'b000 : ;//SEQI Set On Equal To Immediate
            3'b001 : ;//SNEI Set Not Equal To Immediate
            3'b010 : ;//SLTI Set On Less Than Immediate
            3'b011 : ;//SGTI Set Greater Than Immediate
            3'b100 : ;//SLEI Set Less Than Or Equal To Immediate
            3'b101 : ;//SGEI Set Greater Than Or Equal To Immediate
          endcase   
        end
        3'b100 : begin
           memtreg = 1;
           regwrt = 1;
          case(opcode[3:5])
            3'b000 : begin//LB Load Byte
		   dsize = 0;
		   sign = 1;
		end
            3'b001 : begin//LH Load Halfword
		   dsize = 1;
		   sign = 1;
		end
            3'b011 : begin//LW Load Word
            	  dsize = 3;
		end
	    3'b100 : begin//LBU Load Byte Unsigned
		 dsize = 0;
		 sign = 0;
		end
            3'b101 : begin//LHU Load Halfword Unsigned
		 dsize = 1;
		 sign = 0;
		end
          endcase
        end
        3'b101 : begin
           memwrt = 1; //Write to Memory
           rs2 = instru[11:15];
          case(opcode[3:5])
            3'b000 : begin//SB Store Byte
		 dsize = 0;
		end
            3'b001 : begin//SH Store Halfword
		 dsize = 1;
		end
            3'b011 : begin//SW Store Word
		 dsize = 3;
		end
          endcase
        end
      endcase
    end
  //R-type Instructions
    3'b010 : begin       
       rs1[0:4] = instru[6:10];
       rs2[0:4] = instru[11:15];
       rd[0:4] = instru[16:20];
       func[0:5] = instru[26:31]; //ALU functions
       imm16 = 0;
       name = 0;
       imminstr = 0;
       jumpinstr = 0;
       rinstr = 1;
       sign = 0;
       shiftinstr = 0;
       shiftarith = 0;
       shiftdir = 0;
       branchinstr = 0;
       branchop = 0;
       setinstr = 0;
       setop = 0;
       memwrt = 0;
       memtreg = 0;
       regwrt =0;
       multinstr = 0;
      case(func[0:2])
        3'b000 : begin
          case(func[3:5])
            3'b100 : begin //SLL Shift Left Logical
                        shiftdir = 0;
                        shiftarith = 0;
                        memwrt = 1;
                        shiftinstr = 1;
                    end
            3'b110 : begin //SRL Shift Right Logical
                        shiftdir = 1;
                        shiftarith = 0;
                        memwrt = 1;
                        shiftinstr = 1;
                    end
            3'b111 : begin //SRA Shift Right Arithmetic
                       shiftdir = 1;
                       shiftarith = 1;
                        memwrt = 1;
                        shiftinstr = 1;
                    end
          endcase
        end
	3'b001 : begin //MULT Integer Multiply (Signed)
		       multinstr = 1;
	               sign = 1;
		end
        3'b010 : begin
          case(func[3:5])
	       3'b101 : begin
		//NOP
	 	end
	       3'b110 : begin //MULTU Integer Multiply (Unsigned)
		         multinstr = 1;
			 sign = 0;	
		end
	     endcase
          end
        3'b100 : begin
           regwrt = 1;
          case(func[3:5])
            3'b000 : begin//ADD Integer Add (Signed)
                       aluop = 3;
		       sign = 1;
                    end
            3'b001 : begin//ADDU Integer Add Unsigned
		       aluop = 3;
		       sign = 0;
	            end		
            3'b010 : begin//SUB Integer Subtract (Signed)
                       aluop = 7;
		       sign = 1;
                    end
            3'b011 : begin//SUBU Integer Subtract Unsigned
		       aluop = 7;
		       sign = 0;
		    end
            3'b100 : begin//AND Logical And
                       aluop = 0;
                    end
            3'b101 : begin//OR Logical Or
                       aluop = 1;
                    end
            3'b110 : begin//XOR Logical Xor
                       aluop = 2;
                    end
          endcase
        end 
        3'b101 : begin
           setinstr = 1;
           setop = func[3:5];
          case(func[3:5])
            3'b000 : ;//SEQ Set On Equal
            3'b001 : ;//SNE Set On Not Equal
            3'b010 : ;//SLT Set On Less Than
            3'b011 : ;//SGT Set On Greater Than
            3'b100 : ;//SLE Set On Less Than Or Equal
            3'b101 : ;//SGE Set On Greater Than Or Equal
          endcase
        end   
       /* 3'b110 : begin
           regwrt = 1;
          case(func[3:5])
            3'b100 : ;//MOVFP2I Move SPFP To Integer (Single Precision Floating Point)
            3'b101 : ;//MOVI2FP Move Integer TO SPFP
          endcase
        end*/   
      endcase
    end
  //J-type Instructions
    3'b001 : begin
       name[0:25] = instru[6:31];
       imm16 = 0;
       rs1 = 0;
       rs2 = 0;
       rd = 0;
       imminstr = 0;
       jumpinstr = 1;
       rinstr = 0;
       sign = 0;
       shiftinstr = 0;
       shiftarith = 0;
       shiftdir = 0;
       branchinstr = 0;
       branchop = 0;
       setinstr = 0;
       setop = 0;
       memwrt = 0;
       memtreg = 0;
       regwrt = 0;
       aluop = 3;
       multinstr = 0;
      case(opcode)
        6'h02 : begin//J Jump
	     regwrt = 0;
	  end
        6'h03 : begin //JAL Jump And Link
             regwrt = 1;
	     rd = 'd31;
          end
      endcase
    end
  endcase
endmodule //Decoder
