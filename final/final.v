module final(
  input clk, rst, 
  output [0:31] a, b, aluout, memout, instr
  );

  //Wires
  wire if_wrt, ifid_wrt, idex_wrt, exmem_wrt, memwb_wrt; //write flag for registers
  wire [0:31] dontcare;  //for holding misc don't cares
  //IF
  wire [0:31] if_pc_next, if_pc_in, if_instr;
  wire if_pc_src;
  //ID
  wire [0:31] id_instr, id_ra, id_rb, id_imm, id_pc_next;
  wire [0:25] id_name;
  wire [0:15] id_imm16;
  wire [0:4] id_rs1, id_rs2, id_rd;
  wire [0:2] id_aluop, id_setop;
  wire [0:1] id_dsize;
  wire id_imminstr, id_setinstr, id_shiftinstr, id_jumpinstr, id_rinstr, id_branchinstr, id_multinstr;
  wire id_sign, id_shiftdir, id_shiftarith, id_branchop, id_regwrt, id_memwrt, id_memtreg;
  //EX
  wire [0:31] ex_ra, ex_rb, ex_ra_in, ex_rb_in, ex_ra_in_haz, ex_rb_in_haz, ex_imm, ex_pc_next, ex_aluout, ex_pc_dest, ex_pc_in;
  wire [0:4] ex_rd, ex_rs1, ex_rs2;
  wire [0:2] ex_aluop, ex_setop;
  wire [0:1] ex_dsize;
  wire ex_imminstr, ex_setinstr, ex_shiftinstr, ex_jumpinstr, ex_rinstr, ex_branchinstr, ex_multinstr;
  wire ex_sign, ex_shiftdir, ex_shiftarith, ex_branchop, ex_regwrt, ex_memwrt, ex_memtreg, ex_zf;
  wire ex_pipe_regwrt, ex_pipe_memwrt;
  //MEM
  wire [0:31] mem_rb, mem_pc_in, mem_aluout, mem_memout;
  wire [0:4] mem_rd;
  wire [0:1] mem_dsize;
  wire mem_branchinstr, mem_jumpinstr, mem_branchop, mem_zf, mem_memwrt, mem_regwrt, mem_memtreg;
  //WB
  wire [0:31] wb_memout, wb_aluout, wb_regin;
  wire [0:4] wb_rd;
  wire wb_regwrt, wb_memtreg;

  //Modules
  //IF
  instrfetch INSTRFETCH(.clk(~clk), .PC_reset(rst), .wrt(if_wrt), .PC_src(if_pc_src), .branch_in(if_pc_in), 
    .instru(if_instr), .PC_next(if_pc_next));
  //IF/ID PIPE
  ifidpipe PIPE_IF_ID(.clk(clk), .rst(rst), .wrt(ifid_wrt), .in_instr(if_instr), .in_pcnext(if_pc_next),
    .out_instr(id_instr), .out_pcnext(id_pc_next));
  //ID
  decoder DECODER(.instru(id_instr), 
    .rs1(id_rs1), .rs2(id_rs2), .rd(id_rd), .imm16(id_imm16), .aluop(id_aluop), .name(id_name), 
    .imminstr(id_imminstr), .sign(id_sign), .jumpinstr(id_jumpinstr), .rinstr(id_rinstr), 
    .branchinstr(id_branchinstr), .branchop(id_branchop), .regwrt(id_regwrt), .memwrt(id_memwrt), 
    .memtreg(id_memtreg), .dsize(id_dsize), .setinstr(id_setinstr), .setop(id_setop), 
    .shiftinstr(id_shiftinstr), .shiftdir(id_shiftdir), .shiftarith(id_shiftarith), 
    .multinstr(id_multinstr)); 
  regfile REGISTERS(.clk(~clk), .reset(rst), .write(wb_regwrt), .wAddr(wb_rd), .wData(wb_regin), 
    .rAddr1(id_rs1), .rData1(id_ra),.rAddr2(id_rs2), .rData2(id_rb));
  extender SIGNEXTEND(.imm16(id_imm16), .name(id_name), .imminstr(id_imminstr), .immext(id_sign), .ext_val(id_imm));
  //ID/EX PIPE
  idexpipe PIPE_ID_EX(.clk(clk), .rst(rst), .wrt(idex_wrt), 
    .in_ra(id_ra), .in_rb(id_rb), .in_imm(id_imm), .in_pcnext(id_pc_next), .in_wrtd(id_rd), .in_aluop(id_aluop),
    .in_setop(id_setop), .in_branchop(id_branchop), .in_sign(id_sign), .in_shiftdir(id_shiftdir), 
    .in_shiftarith(id_shiftarith), .in_imminstr(id_imminstr), .in_jumpinstr(id_jumpinstr), .in_branchinstr(id_branchinstr), 
    .in_setinstr(id_setinstr), .in_shiftinstr(id_shiftinstr), .in_multinstr(id_multinstr), .in_dsize(id_dsize), 
    .in_memwrt(id_memwrt), .in_regwrt(id_regwrt), .in_memtreg(id_memtreg), .in_rinstr(id_rinstr), 
    .in_rs1(id_rs1), .in_rs2(id_rs2), 
    .out_ra(ex_ra), .out_rb(ex_rb), .out_imm(ex_imm), .out_pcnext(ex_pc_next), .out_wrtd(ex_rd), .out_aluop(ex_aluop),
    .out_setop(ex_setop), .out_branchop(ex_branchop), .out_sign(ex_sign), .out_shiftdir(ex_shiftdir),
    .out_shiftarith(ex_shiftarith), .out_imminstr(ex_imminstr), .out_jumpinstr(ex_jumpinstr), .out_branchinstr(ex_branchinstr),
    .out_setinstr(ex_setinstr), .out_shiftinstr(ex_shiftinstr), .out_multinstr(ex_multinstr), .out_dsize(ex_dsize),
    .out_memwrt(ex_pipe_memwrt), .out_regwrt(ex_pipe_regwrt), .out_memtreg(ex_memtreg), .out_rinstr(ex_rinstr),
    .out_rs1(ex_rs1), .out_rs2(ex_rs2));
  //EX
  topalu ALU(.ra(ex_ra_in_haz),.rb(ex_rb_in_haz),.imm(ex_imm), .setinstr(ex_setinstr), .imminstr(ex_imminstr), 
    .shiftinstr(ex_shiftinstr), .aluop(ex_aluop), .setop(ex_setop), .shiftarith(ex_shiftarith), .shiftdir(ex_shiftdir), 
    .out(ex_aluout), .zf(ex_zf));
  fa32 BRANCHCALC(.A(ex_pc_next), .B(ex_imm), .cin(1'b0), .Sum(ex_pc_dest), .cout(dontcare[0]));
  assign ex_regwrt = ex_pipe_regwrt & ~stall_pipe;
  assign ex_memwrt = ex_pipe_memwrt & ~stall_pipe;
  //EX/MEM PIPE
  exmempipe PIPE_EX_MEM(.clk(clk), .rst(rst), .wrt(exmem_wrt),
    .in_pc_in(ex_pc_in), .in_aluout(ex_aluout), .in_rb(ex_rb), .in_wrtd(ex_rd), .in_zf(ex_zf),
    .in_branchinstr(ex_branchinstr), .in_branchop(ex_branchop), .in_jumpinstr(ex_jumpinstr), 
    .in_dsize(ex_dsize), .in_memwrt(ex_memwrt), .in_regwrt(ex_regwrt), .in_memtreg(ex_memtreg),
    .out_pc_in(mem_pc_in), .out_aluout(mem_aluout), .out_rb(mem_rb), .out_wrtd(mem_rd), .out_zf(mem_zf),
    .out_branchinstr(mem_branchinstr), .out_branchop(mem_branchop), .out_jumpinstr(mem_jumpinstr),
    .out_dsize(mem_dsize), .out_memwrt(mem_memwrt), .out_regwrt(mem_regwrt), .out_memtreg(mem_memtreg));
  //MEM
  dmem DATA_MEM(.clk(~clk), .addr(mem_aluout), .rData(mem_memout), .wData(mem_rb), .writeEnable(mem_memwrt), .dsize(mem_dsize));
  assign if_pc_src = (((mem_zf ^ mem_branchop) & mem_branchinstr) | mem_jumpinstr);
  assign if_pc_in = mem_pc_in;
  //MEM/WB PIPE
  memwbpipe PIPE_MEM_WB(.clk(clk), .rst(rst), .wrt(memwb_wrt),
    .in_memout(mem_memout), .in_aluout(mem_aluout), .in_wrtd(mem_rd), .in_regwrt(mem_regwrt), .in_memtreg(mem_memtreg),
    .out_memout(wb_memout), .out_aluout(wb_aluout), .out_wrtd(wb_rd), .out_regwrt(wb_regwrt), .out_memtreg(wb_memtreg));
  //WB
  //Yay no main components
  
  //Multiplexes
  genvar i;
  wire [0:31] four;
  assign four = 4;
  generate
    for(i=0;i<32;i=i+1) begin : muxes
      //EX
      mux21 JALRA(ex_ra[i], ex_pc_next[i], ex_regwrt & ex_jumpinstr, ex_ra_in[i]);
      mux21 JALRB(ex_rb[i], four[i], ex_regwrt & ex_jumpinstr, ex_rb_in[i]);
      mux21 PC_DEST(ex_pc_dest[i], ex_rb[i], ex_rinstr, ex_pc_in[i]);
      //WB
      mux21 MEMTOREG(wb_aluout[i], wb_memout[i], wb_memtreg, wb_regin[i]);
      //Hazard forwarding
      mux31 RA_IN(ex_ra_in[i], mem_aluout[i], wb_regin[i], forwarda, ex_ra_in_haz[i]);
      mux31 RB_IN(ex_rb_in[i], mem_aluout[i], wb_regin[i], forwardb, ex_ra_in_haz[i]);
    end
  endgenerate

  //Hazard Detection
  //forwarding
  wire [0:1] forwarda, forwardb;
  wire mem_rd_iszero, wb_rd_iszero;
  wire mem_rd_is_ex_rs1, mem_rd_is_ex_rs2;
  wire wb_rd_is_ex_rs1, wb_rd_is_ex_rs2;
  iszero MEMRDZERO(mem_rd, mem_rd_iszero),
          WBRDZERO(wb_rd, wb_rd_iszero);
  isequal MEMRD_EXRS1(mem_rd, ex_rs1, mem_rd_is_ex_rs1),
          MEMRD_EXRS2(mem_rd, ex_rs2, mem_rd_is_ex_rs2),
           WBRD_EXRS1(wb_rd, ex_rs1, wb_rd_is_ex_rs1),
           WBRD_EXRS2(wb_rd, ex_rs2, wb_rd_is_ex_rs2);
  assign forwarda[1] = mem_regwrt & ~mem_rd_iszero & mem_rd_is_ex_rs1;
  assign forwardb[1] = mem_regwrt & ~mem_rd_iszero & mem_rd_is_ex_rs2;
  assign forwarda[0] = wb_regwrt & ~wb_rd_iszero & ~forwarda[1] & wb_rd_is_ex_rs1;
  assign forwardb[0] = wb_regwrt & ~wb_rd_iszero & ~forwardb[1] & wb_rd_is_ex_rs2;
  //bubbling
  wire stall_pipe;
  wire ex_rd_is_id_rs1, ex_rd_is_id_rs2;
  isequal EXRD_IDRS1(ex_rd, id_rs1, ex_rd_is_id_rs1),
          EXRD_IDRS2(ex_rd, id_rs2, ex_rd_is_id_rs2);
  //@FIXME assign stall_pipe = (ex_memtreg & (ex_rd_is_id_rs1 | ex_rd_is_id_rs2)) | (id_branchinstr & ~mem_branchinstr);
  assign stall_pipe = 0;
  //Write logic
  assign if_wrt = ~stall_pipe;
  assign ifid_wrt = ~stall_pipe;
  assign idex_wrt = 1;
  assign exmem_wrt = 1;
  assign memwb_wrt = 1;

  //Outputs
  assign a = ex_ra;
  assign b = ex_rb;
  assign aluout = mem_aluout;
  assign memout = wb_memout;
  assign instr = id_instr;
endmodule 

module iszero(
    input [0:4] in,
    output zf
    );

    assign zf = in[0] | in[1] | in[2] | in[3] | in[4];
endmodule

module isequal(
    input [0:4] a, b,
    output zf
    );

    wire [0:4] res;
    genvar i;
    generate
        for(i=0;i<5;i=i+1) begin : xors
            assign res[i] = a[i] ^ b[i];
        end
    endgenerate
    iszero ZEROCHECK(res, zf);
endmodule
