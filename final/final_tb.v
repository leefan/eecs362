/*Single Cycle Testbench
Stephen Zeng*/
module singlecycle_tb;
  parameter IMEMFILE = "instr.hex";
  parameter DMEMFILE = "data.hex"; 
  reg [8*80-1:0] filename;
  reg clk, reset;
  wire [0:31] a, b, aluout, memout, instr;

  final CPU(.clk(clk), .rst(reset), .a(a), .b(b), .aluout(aluout), .memout(memout), .instr(instr));

  integer i;

  always
  #5 clk = !clk;
  initial begin
	  // Clear DMEM
	  for (i = 0; i < CPU.DATA_MEM.SIZE; i = i+1)
	        CPU.DATA_MEM.mem[i] = 8'h0;
	  //Load Instruction memory Hexfile
	  if (!$value$plusargs("instrfile=%s", filename)) begin
	            filename = IMEMFILE;
	  end
	  $readmemh(filename, CPU.INSTRFETCH.Instrmem.mem);
	  // Load DMEM from file
	  if (!$value$plusargs("datafile=%s", filename)) begin
		    filename = DMEMFILE;
	  end
	  $readmemh(filename, CPU.DATA_MEM.mem);
	  //Monitor Outputs
    clk = 0; reset = 0;
    #5 reset = 1;
    #6000 $finish;
  end
  always @(posedge clk or negedge clk) $strobe("PC_VAL: %h A: %h B: %h ALUOUT: %h MEMOUT: %h INSTR: %h CLK: %d Time: %d\n  IFIDPIP: %h %h\n  IDEX: %h %h %h %h %b %b %b %b%b%b%b%b%b%b%b%b%b %b %b%b%b%b\n  EXMEM: %h %h %h %b %b%b%b%b %b %b%b%b\n", 
        CPU.INSTRFETCH.PC_val_reg, a, b, aluout, memout, instr, clk, $time, CPU.id_instr, CPU.id_pc_next,
        CPU.ex_ra, CPU.ex_rb, CPU.ex_imm, CPU.ex_pc_next, CPU.ex_rd, CPU.ex_aluop, CPU.ex_setop, CPU.ex_branchop,
        CPU.ex_sign, CPU.ex_shiftdir, CPU.ex_shiftarith, CPU.ex_imminstr, CPU.ex_jumpinstr, CPU.ex_branchinstr, 
        CPU.ex_setinstr, CPU.ex_shiftinstr, CPU.ex_multinstr, CPU.ex_dsize, CPU.ex_memwrt, CPU.ex_regwrt, 
        CPU.ex_memtreg, CPU.ex_rinstr,
        CPU.mem_pc_in, CPU.mem_aluout, CPU.mem_rb, CPU.mem_rd, CPU.mem_zf, CPU.mem_branchinstr, 
        CPU.mem_branchop, CPU.mem_jumpinstr, CPU.mem_dsize, CPU.mem_memwrt, CPU.mem_regwrt, CPU.mem_memtreg);
endmodule
