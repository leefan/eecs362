module multiplier(
  input clk, rst, 
  input [0:31] m, r, 
  output [0:63] out, 
  output done //1 when complete
  /*/TO/FROM ALU
  input [0:31] aluout,
  output [0:2] aluop,
  output [0:31] P,*/
  ); 
  
  wire [0:1] sel;
  wire zf,cf,of,sf; //placeholders
  wire [0:64] SHin, SHout;
  wire [0:31] Pin, Pout, Ain, Aout, A, dontcare;
  wire Sin, Sout;
  wire [0:5] Cin, Cout;
  wire [0:31] sum, dif;
  //Code Start
  register REGP(clk, rst, 1, Pin, Pout),
           REGA(clk, 1, 1, A, Aout);
  register #(.width(1)) REGS(clk, rst, 1, Sin, Sout);
  register #(.width(6)) COUNT(clk, rst, 1, Cin, Cout);
  register #(.width(65)) SHIFTRES(~clk, rst, 1, SHin, SHout);

  assign sel = { Aout[31], Sout };

  genvar i;
  generate
  	for (i=0;i<65;i=i+1) begin : shifting
      if (i==0)
        mux41 MUX41(Pout[i],sum[i],dif[i],Pout[i],sel,SHin[i]);
      else if (i<33)
  	    mux41 MUX41(Pout[i-1],sum[i-1],dif[i-1],Pout[i-1],sel,SHin[i]);
  	  else
  	    assign SHin[i] = Aout[i-33];
  	end
    for (i=0;i<32;i=i+1) begin : Ainit
      mux21 INITA(r[i], Ain[i], rst, A[i]);
    end
  endgenerate

  assign Pin = SHout[0:31];
  assign Ain = SHout[32:63];
  assign Sin = SHout[64];
  
  fa32 #(.WIDTH(6)) inc(Cout, 1, 1'b0, Cin, cf); //make incrementer to save space here
  
  alu32 add(Pout, m, 3, sum, zf, cf, of, sf);
  alu32 sub(Pout, m, 7, dif, zf, cf, of, sf);
  
  assign out = { Pout, Aout };
  assign done = Cout[0];

endmodule

/*
module testbench;
  reg clk, rst;
  reg[0:31] m, r;
  wire [0:63] out;
  wire done;

  multiplier MULT(clk, rst, m, r, out, done);

  initial begin
    $monitor("out: %d done: %d at time=%t", out, done, $time);
    clk=0;
    $display("first example: a = 3 b = 17");
    m = 3; r = 17; rst = 0; #50 rst = 1;
    #320 $display("first example done");
    $display("second example: a = 7 b = 7");
    m = 7; r = 7; rst = 0; #50 rst = 1;
    #320 $display("second example done");
    $finish;
  end

  always #5 clk = !clk;

endmodule
*/
