module mux321(
    input [0:31] in, //big endian bs...
    input [0:4] ctrl,
    output out
    );

    wire outa, outb, outc, outd;

    mux81 MUXA(in[31], in[30], in[29], in[28], in[27], in[26], in[25], in[24], ctrl[2:4], outa);
    mux81 MUXB(in[23], in[22], in[21], in[20], in[19], in[18], in[17], in[16], ctrl[2:4], outb);
    mux81 MUXC(in[15], in[14], in[13], in[12], in[11], in[10], in[9], in[8], ctrl[2:4], outc);
    mux81 MUXD(in[7], in[6], in[5], in[4], in[3], in[2], in[1], in[0], ctrl[2:4], outd);

    mux41 MAIN(outa, outb, outc, outd, ctrl[0:1], out);
endmodule

/*
module testbench;
    reg [0:31] in;
    reg [0:4] ctrl;
    wire out;
    integer i, j;

    mux321 MUXTEST(in, ctrl, out);
    initial begin
        $monitor("In: %b %b Ctrl: %d(%b) Out: %b ABCD: %b%b%b%b", in[16:23], in[24:31], ctrl, ctrl, out, MUXTEST.outa, MUXTEST.outb, MUXTEST.outc, MUXTEST.outd);
        in=0;
        for(i=0;i<65535;i=i+1) begin
            ctrl=0;
            for(j=0;j<31;j=j+1) begin
                #1 ctrl=ctrl+1;
            end
            #1 in=in+1;
        end
    end
endmodule
*/
