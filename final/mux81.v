/* Stephen Zeng
8 by 1 Multiplexer*/ 
`timescale 1ns/1ps
module mux81(
	in0, 
	in1, 
	in2, 
	in3, 
	in4, 
	in5, 
	in6, 
	in7, 
	sel, 
	out);
	//Inputs
	input in0, in1, in2, in3, in4, in5, in6, in7;
	//Muxops
	input [0:2] sel;
	//Output
	output out;
	//Code Start
	wire mout_a, mout_b, aout_a, aout_b;
	mux41 m0(.in0(in0), .in1(in1), .in2(in2), .in3(in3), .sel(sel[1:2]), .out(mout_a)),
          m1(.in0(in4), .in1(in5), .in2(in6), .in3(in7), .sel(sel[1:2]), .out(mout_b));
	assign aout_a = mout_a & ~sel[0];
	assign aout_b = mout_b & sel[0];
	assign out = aout_a | aout_b;
endmodule //mux81
