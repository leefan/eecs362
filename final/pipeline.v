module ifidpipe(
    input clk, rst, wrt,
    input [0:31] in_instr, in_pcnext,
    output [0:31] out_instr, out_pcnext
    );

    register INSTRUCTION(clk, rst, wrt, in_instr, out_instr),
                 PC_NEXT(clk, rst, wrt, in_pcnext, out_pcnext);
endmodule

module idexpipe(
    input clk, rst, wrt,
    input [0:31] in_ra, in_rb, in_imm, in_pcnext,
    input [0:4] in_wrtd, 
    //EX controls
    input [0:2] in_aluop, in_setop,
    input in_sign, in_branchop, in_shiftdir, in_shiftarith,
    input in_imminstr, in_rinstr, in_jumpinstr, in_branchinstr, in_setinstr, in_shiftinstr, in_multinstr, 
    //MEM controls
    input [0:1] in_dsize, 
    input in_memwrt, 
    //WB controls
    input in_regwrt, in_memtreg, 
    //for hazard detection
    input [0:4] in_rs1, in_rs2,

    //output
    output [0:31] out_ra, out_rb, out_imm, out_pcnext,
    output [0:4] out_wrtd,
    //EX
    output [0:2] out_aluop, out_setop,
    output out_sign, out_branchop, out_shiftdir, out_shiftarith,
    output out_imminstr, out_rinstr, out_jumpinstr, out_branchinstr, out_setinstr, out_shiftinstr, out_multinstr,
    //MEM
    output [0:1] out_dsize, out_pcsrc,
    output out_memwrt,
    //WB
    output out_regwrt, out_memtreg,
    //for hazard detection
    output [0:4] out_rs1, out_rs2
    );

    register RA(clk, rst, wrt, in_ra, out_ra),
             RB(clk, rst, wrt, in_rb, out_rb),
            IMM(clk, rst, wrt, in_imm, out_imm),
        PC_NEXT(clk, rst, wrt, in_pcnext, out_pcnext);
    
    register #(.width(5)) WRTD(clk, rst, wrt, in_wrtd, out_wrtd),
                           RS1(clk, rst, wrt, in_rs1, out_rs1),
                           RS2(clk, rst, wrt, in_rs2, out_rs2);

    register #(.width(3)) ALUOP(clk, rst, wrt, in_aluop, out_aluop),
                          SETOP(clk, rst, wrt, in_setop, out_setop);

    register #(.width(2)) DSIZE(clk, rst, wrt, in_dsize, out_dsize);

    register #(.width(1)) SIGN(clk, rst, wrt, in_sign, out_sign),
                      BRANCHOP(clk, rst, wrt, in_branchop, out_branchop),
                      SHIFTDIR(clk, rst, wrt, in_shiftdir, out_shiftdir),
                    SHIFTARITH(clk, rst, wrt, in_shiftarith, out_shiftarith),

                      IMMINSTR(clk, rst, wrt, in_imminstr, out_imminstr),
                        RINSTR(clk, rst, wrt, in_rinstr, out_rinstr),
                     JUMPINSTR(clk, rst, wrt, in_jumpinstr, out_jumpinstr),
                   BRANCHINSTR(clk, rst, wrt, in_branchinstr, out_branchinstr),
                      SETINSTR(clk, rst, wrt, in_setinstr, out_setinstr),
                    SHIFTINSTR(clk, rst, wrt, in_shiftinstr, out_shiftinstr),
                     MULTINSTR(clk, rst, wrt, in_multinstr, out_multinstr),

                        MEMWRT(clk, rst, wrt, in_memwrt, out_memwrt),

                        REGWRT(clk, rst, wrt, in_regwrt, out_regwrt),
                       MEMTREG(clk, rst, wrt, in_memtreg, out_memtreg);
endmodule

module exmempipe(
    input clk, rst, wrt,
    input [0:31] in_pc_in, in_aluout, in_rb,
    input [0:4] in_wrtd,
    input in_zf, in_branchop, in_branchinstr, in_jumpinstr,
    //MEM
    input [0:1] in_dsize, 
    input in_memwrt, 
    //WB
    input in_regwrt, in_memtreg,

    //output
    output [0:31] out_pc_in, out_aluout, out_rb,
    output [0:4] out_wrtd,
    output out_zf, out_branchop, out_branchinstr, out_jumpinstr,
    //MEM
    output [0:1] out_dsize, out_pcsrc,
    output out_memwrt,
    //WB
    output out_regwrt, out_memtreg
    );

    register   PCIN(clk, rst, wrt, in_pc_in, out_pc_in),
             ALUOUT(clk, rst, wrt, in_aluout, out_aluout),
                 RB(clk, rst, wrt, in_rb, out_rb);

    register #(.width(5)) WRTD(clk, rst, wrt, in_wrtd, out_wrtd);

    register #(.width(2)) DSIZE(clk, rst, wrt, in_dsize, out_dsize);

    register #(.width(1)) ZF(clk, rst, wrt, in_zf, out_zf),
                    BRANCHOP(clk, rst, wrt, in_branchop, out_branchop),
                 BRANCHINSTR(clk, rst, wrt, in_branchinstr, out_branchinstr),
                   JUMPINSTR(clk, rst, wrt, in_jumpinstr, out_jumpinstr),

                      MEMWRT(clk, rst, wrt, in_memwrt, out_memwrt),

                      REGWRT(clk, rst, wrt, in_regwrt, out_regwrt),
                     MEMTREG(clk, rst, wrt, in_memtreg, out_memtreg);
endmodule

module memwbpipe(
    input clk, rst, wrt,
    input [0:31] in_memout, in_aluout, 
    input [0:4] in_wrtd,
    //WB
    input in_regwrt, in_memtreg,

    //output
    output [0:31] out_memout, out_aluout,
    output [0:4] out_wrtd,
    //WB
    output out_regwrt, out_memtreg
    );

    register MEMOUT(clk, rst, wrt, in_memout, out_memout),
             ALUOUT(clk, rst, wrt, in_aluout, out_aluout);

    register #(.width(5)) WRTD(clk, rst, wrt, in_wrtd, out_wrtd);

    register #(.width(1)) REGWRT(clk, rst, wrt, in_regwrt, out_regwrt),
                         MEMTREG(clk, rst, wrt, in_memtreg, out_memtreg);
endmodule

