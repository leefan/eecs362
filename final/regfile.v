/* behavioral code: */

/*
module regfile
    (input clk,
     input reset,
     input write,
     input [0:4] wAddr,
     input [0:31] wData,
     input [0:4] rAddr1,
     output [0:31] rData1,
     input [0:4] rAddr2,
     output [0:31] rData2
    );

    reg [0:31] regs [0:31];

    assign rData1 = regs[rAddr1];
    assign rData2 = regs[rAddr2];

    integer i;

    always @(posedge clk) begin
        regs[0] <= 0;
        if(reset) begin
            for(i=0;i<32;i=i+1) begin
                regs[i] <= 0;
            end
        end else begin
            if (write) regs[wAddr] <= wData;
        end
    end
endmodule
*/

module regfile(
    input clk, reset, write,
    input [0:4] wAddr,
    input [0:31] wData,
    input [0:4] rAddr1,
    output [0:31] rData1,
    input [0:4] rAddr2,
    output [0:31] rData2
    );
    
    wire [0:31] regout [0:31], muxin [0:31];
    wire [0:31] regcontrol;

    decd532 WRITEDECODE(wAddr, regcontrol);
    
    genvar i, j;
    generate
        for (i=0;i<32;i=i+1) begin : registerlogicblock
            for (j=0;j<32;j=j+1) begin : muxinrearrange
                assign muxin[i][j] = regout[j][i];
            end
            if(i==31)
                register REGISTER(clk, reset, (write & regcontrol[i]), 0, regout[i]);
            else
                register REGISTER(clk, reset, (write & regcontrol[i]), wData, regout[i]);
            mux321 MUXA(muxin[i], rAddr1, rData1[i]);
            mux321 MUXB(muxin[i], rAddr2, rData2[i]);
        end
    endgenerate
endmodule

module register(clk, rst, wrt, in, out);
    parameter width = 32;
    parameter init = 0;
    input clk, rst, wrt;
    input [0:width-1] in;
    output [0:width-1] out;

    wire [0:width-1] val;
    
    genvar i;
    generate
        for (i=0;i<width;i=i+1) begin : registergeneration
            mux21 REGMUX(out[i], in[i], wrt, val[i]);
        end
    endgenerate

    PipeReg #(.width(width), .init(init)) PIPE(out, val, clk, rst);
endmodule

/*
module testbench;
    reg clk, rst, wrt;
    reg [0:4] w, r1, r2;
    reg [0:31] in;
    wire [0:31] out1, out2;

    regfile REGFILE(clk, rst, wrt, w, in, r1, out1, r2, out2);

    always
        #5 clk=~clk;
    initial begin
        $monitor("Time: %d\n rst,wrt: %b%b write: %d(%d) read: %d(%d), %d(%d)", $time, rst, wrt, w, in, r1, out1, r2, out2); 
        clk=0;rst=0;
        #10 rst=1; wrt=1; w=3; r1=0; r2=5; in=20;
        #10 wrt=1; w=5; r1=3; r2=4; in=30;
        #10 wrt=1; w=0; r1=3; r2=5; in=40;
        #10 wrt=0; w=6; r1=0; r2=5; in=50;
        #10 wrt=0; w=2; r1=0; r2=6; in=60;
        #10 wrt=1; w=31; r1=0; r2=2; in=70;
        #10 wrt=0; w=30; r1=31; r2=0; in=80;
        #10 $finish;
    end
endmodule
*/
