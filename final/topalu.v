/* Stephen Zeng
 * Lee Fan
Top Level ALU w/ comparison functions*/
module topalu(
  input [0:31] ra, rb,
  input [0:31] imm,
  input setinstr, imminstr, shiftinstr,
  input [0:2] aluop, setop,
  input shiftarith, shiftdir,
  output [0:31] out,
  output zf);  //zero flag

  //Code Begin
  wire result [0:31]; //holds result to be loaded into register after ALU
  
  //alu input mux
  wire [0:31] ina; //First input can be jump increment or register
  wire [0:31] inb; //second input can be immediate or register
  genvar i;
  generate
  	for (i = 0; i < 32; i = i+1) begin : inputb
    		mux21 secondinput(.in0(rb[i]), .in1(imm[i]), .sel(imminstr), .out(inb[i]));
  	end
  endgenerate
  
  //shift operations
  wire [0:31] shiftres;
  shifter SHIFTER(.in(ra), .offset(inb), .arith(shiftarith), .direction(~shiftdir), .out(shiftres)); 
  
  //ALU stuff
  wire cf, of, sf; //flags
  wire [0:31] alures; //result from ALU
  alu32 ALU32(.a(ra), .b(inb), .op(aluop), .res(alures), .zf(zf), .cf(cf), .of(of), .sf(sf));
  
  //SET instruction processing
  wire setres; //results of set instruction
  wire [0:31] setresext; //setres extended
  wire seq, sne, slt, sgt, sle, sge; //Set bits
  wire dum0, dum1; //Dummy Values
  assign seq = zf;
  assign sne = ~zf;
  assign slt = sf ^ of;
  assign sle = slt | zf;
  assign sgt = ~sle;
  assign sge = ~slt;
  mux81 MUX81(.in0(seq), .in1(sne), .in2(slt), .in3(sgt), .in4(sle), .in5(sge), 
              .in6(dum0), .in7(dum1), .sel(setop), .out(setres)); 
  assign setresext = { {31{1'b0}}, setres };
  
  //final result
  generate
  	for (i = 0; i < 32; i = i + 1) begin : block
  		mux31 finalalu(.in0(alures[i]), .in1(setresext[i]), .in2(shiftres[i]), .sel({setinstr, shiftinstr}), .out(out[i]));
  	end
  endgenerate
  
endmodule
