`timescale 1ns/1ps
module flagdet(
	zf,
	cf,
	of,
	sf);
	input zf; //Zero Flag
	input cf; //Carry Flag
	input of; //Overflow Flag
	input sf; //Sign Flag
	
