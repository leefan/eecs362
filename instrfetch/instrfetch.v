/* Stephen Zeng 
Instruction Fetch */
`timescale 1ns/1ps
module instrfetch(
  input clk, PC_reset, wrt, PC_src,
  input [0:31] branch_in, //Jump and Branch destinations
  output [0:31] instru, PC_next
  );

  //Variables
  wire [0:31] PC_val_in, PC_val_reg, PC_val_incr;
  wire dummy; //Dummy variables
  //Code start
  genvar i;
  generate
  //Decide on PC Input  
	for (i = 0; i <= 31; i = i + 1) begin : PC_select 
      mux21 PC_val_sel(PC_val_incr[i], branch_in[i], PC_src, PC_val_in[i]);
    end
   endgenerate
  //PC register + Instruction memory fetch
  register PCval (.clk(clk), .rst(PC_reset), .wrt(wrt), .in(PC_val_in), .out(PC_val_reg));
  imem Instrmem (.addr(PC_val_reg), .instr(instru)); //Feed PC Value into Instruction Memory
  //PC increment by 4.
  fa32 incr (.A(PC_val_reg), .B(4), .cin(1'b0), .Sum(PC_val_incr), .cout(dummy));
 //Set Output values 
 assign PC_next = PC_val_incr;

endmodule //Instruction Fetch 
