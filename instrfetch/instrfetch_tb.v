`timescale 1ns/1ps
module instrfetch_tb;
	parameter IMEMFILE = "instr.hex"; 
	reg [8*80-1:0] filename;
	reg clk, reset;
	reg [0:1] sel;
	reg [0:31] rs1, imm32;
	wire [0:31] instru, PC_val, PC_next;
   
	instrfetch INSTRFETCH(.clk(clk), .PC_reset(reset), .PC_src(sel), .rs(rs1), .imm32(imm32), .instru(instru), .PC_val(PC_val), .PC_next(PC_next));

	always
	  #5 clk = !clk;

  	initial begin
      //Load Instructions from hex file
      if (!$value$plusargs("instrfile=%s", filename)) begin
            filename = IMEMFILE;
	  end
      $readmemh(filename, INSTRFETCH.Instrmem.mem);
	
		$monitor("Time = %d Reset = %b Sel = %b RS1 = %h Imm32 = %h Instr = %h PC_val = %h PC_next = %h", $time, reset, sel, rs1, imm32, instru, PC_val, PC_next);
		#0 clk = 0; reset = 0; rs1 = 0; imm32 = 0;
		#5 reset = 1; sel = 0; imm32 = 0;
		#10 reset = 0; sel = 1; imm32 = 0;
		#25 reset = 0; sel = 1; imm32 = 'hab;
		#40 reset = 0; sel = 2; imm32 = 0;
		#60 reset = 0; sel = 2; imm32 = 'hab;
		#70 reset = 0; sel = 3; imm32 = 'hab;
		#80 reset = 0; sel = 4; rs1 = 'h34ab;
		#90 reset = 1; sel = 3; imm32 = 'h34ab;
	end
	initial 
	#100 $finish;

endmodule
