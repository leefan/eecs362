module multiplier(
	m, 
	r, 
	init, //0 if first time 
	out, 
	ready); //0 if complete
  output [0:63] out;
  output ready;
  input [0:31] m, r;
  input init;
  wire [0:1] sel;
  wire zf,cf,of,sf; //placeholders
  wire [0:64] shiftresults;
  wire [0:31] P, A;
  wire S;
  wire [0:4] count;
  wire [0:4] countinc;
  wire [0:31] sum, dif;
  //Code Start
  assign P = P & init;
  assign A = r; //initialize r when init
  assign S = S & init;
  assign count = count & init;
  genvar i;
  generate
  	if (init) begin
  		assign sel[1] = S;
  		assign sel[0] = A[0];
  		for (i=0;i<65;i=i+1)
  		begin
  			if (i==0)
  				mux41 MUX41(P[0],sum[0],dif[0],P[0],sel,shiftresults[0]);
  			else if (i<33)
  				mux41 MUX41(P[i-1],sum[i-1],dif[i-1],P[i-1],sel,shiftresults[i]);
  			else
  				assign shiftresults[i] = A[i-33];
  		end
  	end
  endgenerate
  
  assign P = shiftresults[0:31];
  assign A = shiftresults[32:63];
  assign S = shiftresults[64];
  
  alu32 inc(count, 1, 3, countinc, zf, cf, of, sf); //make incrementer to save space here
  assign count = countinc;
  
  alu32 add(P, m, 3, sum, zf, cf, of, sf);
  alu32 sub(P, m, 7, dif, zf, cf, of, sf);
  
  assign out = {P, A};
  assign ready = (count < 32);

endmodule
