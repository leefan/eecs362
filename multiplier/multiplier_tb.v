`timescale 1ns/1ps
module testbench;
  reg [31:0] a, b;
  reg init;
  wire [63:0] c;
  wire ready;

  multiplier multiply(a, b, init, c, ready);

  initial begin
    $monitor("A = %d B = %d C = %d", a, b, c);

      #5 a = 3; b = 5; start = 1; #50 start = 0;
      #5 a = 5; b = 10; start = 1; #50 start = 0;
  end
endmodule

