`timescale 1ns/1ps
module mux31(
  input in0, in1, in2,
  input [0:1] sel,
  output out);

  wire mux;
  mux21 a0 (in0, in1, sel[1], mux),
	    a2 (mux, in2, sel[0], out);

endmodule //mux31
