
`timescale 1ns/1ps
module mux41(in0, in1, in2, in3, sel, out);
  input in0, in1, in2, in3;
  input [0:1] sel;
  output out;

  wire mux1, mux2;
  mux21 a0 (in0, in1, sel[1], mux1),
	a1 (in2, in3, sel[1], mux2),
	a2 (mux1, mux2, sel[0], out);

endmodule //mux41
