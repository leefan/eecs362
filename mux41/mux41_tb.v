`timescale 1ns/1ps
module testbench;
  reg in0, in1, in2, in3;
  reg [0:1] sel;
  wire out;

  mux41 MUX41(.in0(in0), .in1(in1), .in2(in2), .in3(in3), .sel(sel), .out(out));
  
  initial begin
    $monitor("IN0 = %b IN1 = %b IN2 = %b IN3 = %b SEL = %b OUT = %b", in0, in1, in2, in3, sel, out);
    
    #0 in0 = 0; in1 = 0; in2 = 0; in3 = 0; sel = 0;
    #1 in0 = 1; in1 = 0; in2 = 0; in3 = 0; sel = 0;
    #1 in0 = 1; in1 = 1; in2 = 1; in3 = 1; sel = 0;
    #1 in0 = 0; in1 = 0; in2 = 0; in3 = 1; sel = 0;
    #1 in0 = 0; in1 = 0; in2 = 0; in3 = 0; sel = 1;
    #1 in0 = 0; in1 = 1; in2 = 0; in3 = 0; sel = 1;
    #1 in0 = 1; in1 = 1; in2 = 1; in3 = 1; sel = 1;
    #1 in0 = 0; in1 = 0; in2 = 0; in3 = 0; sel = 2;
    #1 in0 = 0; in1 = 0; in2 = 1; in3 = 0; sel = 2;
    #1 in0 = 1; in1 = 1; in2 = 1; in3 = 1; sel = 2;
    #1 in0 = 0; in1 = 0; in2 = 0; in3 = 0; sel = 3;
    #1 in0 = 0; in1 = 0; in2 = 0; in3 = 1; sel = 3;
    #1 in0 = 1; in1 = 0; in2 = 0; in3 = 0; sel = 3;
    #1 in0 = 1; in1 = 1; in2 = 1; in3 = 1; sel = 3;

  end

endmodule
