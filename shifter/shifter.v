/* Lee Fan
32 Bit Shifter*/
`timescale 1ns/1ps
module shifter(input [0:31] in, offset, input arith, direction, output [0:31] out);
	wire [0:31] sh1, sh2, sh4, sh8;
	wire [0:1] sel1, sel2, sel3, sel4, sel5;
	wire arithshift = in[0] & arith;
	genvar i;
	generate
		assign sel1[1] = offset[31];
		assign sel1[0] = direction;
		for (i=0;i<32;i=i+1)
		begin : shf1
			if(i==0)
				mux41 MUX41(in[i],arithshift,in[i],in[i+1],sel1,sh1[i]);
			else if(i==31)
				mux41 MUX41(in[i],in[i-1],in[i],1'b0,sel1,sh1[i]);
			else
				mux41 MUX41(in[i],in[i-1],in[i],in[i+1],sel1,sh1[i]);
		end
		assign sel2[1] = offset[30];
		assign sel2[0] = direction;
		for (i=0;i<32;i=i+1)
		begin : shf2
			if(i<2)
				mux41 MUX41(sh1[i],arithshift,sh1[i],sh1[i+2],sel2,sh2[i]);
			else if(i>29)
				mux41 MUX41(sh1[i],sh1[i-2],sh1[i],1'b0,sel2,sh2[i]);
			else
				mux41 MUX41(sh1[i],sh1[i-2],sh1[i],sh1[i+2],sel2,sh2[i]);
		end
		assign sel3[1] = offset[29];
		assign sel3[0] = direction;
		for (i=0;i<32;i=i+1)
		begin : shf4
			if(i<4)
				mux41 MUX41(sh2[i],arithshift,sh2[i],sh2[i+4],sel3,sh4[i]);
			else if(i>27)
				mux41 MUX41(sh2[i],sh2[i-4],sh2[i],1'b0,sel3,sh4[i]);
			else
				mux41 MUX41(sh2[i],sh2[i-4],sh2[i],sh2[i+4],sel3,sh4[i]);
		end
		assign sel4[1] = offset[28];
		assign sel4[0] = direction;
		for (i=0;i<32;i=i+1)
		begin : shf8
			if(i<8)
				mux41 MUX41(sh4[i],arithshift,sh4[i],sh4[i+8],sel4,sh8[i]);
			else if(i>23)
				mux41 MUX41(sh4[i],sh4[i-8],sh4[i],1'b0,sel4,sh8[i]);
			else
				mux41 MUX41(sh4[i],sh4[i-8],sh4[i],sh4[i+8],sel4,sh8[i]);
		end
		assign sel5[1] = offset[27];
		assign sel5[0] = direction;
		for (i=0;i<32;i=i+1)
		begin : shf16
			if(i<16)
				mux41 MUX41(sh8[i],arithshift,sh8[i],sh8[i+16],sel5,out[i]);
			else
				mux41 MUX41(sh8[i],sh8[i-16],sh8[i],1'b0,sel5,out[i]);
		end
	endgenerate
endmodule
