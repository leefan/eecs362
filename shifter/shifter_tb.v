`timescale 1ns/1ps
module testbench;
  reg direction, arith;
  reg [0:31] in, offset;
  wire [0:31]  out;
  
  shifter SHIFTER(.in(in), .offset(offset), .arith(arith), .direction(direction), .out(out));

  
  initial begin
   $monitor("IN = %h OFFSET = %h ARI = %b DIR = %b OUT = %h, %b%b%b%b%b", in, offset, arith, direction, out, offset[0], offset[1], offset[2], offset[3], offset[4]);

      in = 'h76543210; offset = 'h88888888; arith = 0; direction = 0;
      #5 in = 'h76543210; offset = 'h88888888; arith = 0; direction = 1;
      #5 in = 'h76543210; offset = 'h4; arith = 0; direction = 0;
      #5 in = 'h76543210; offset = 'h8; arith = 0; direction = 0;
      #5 in = 'h76543210; offset = 'hC; arith = 0; direction = 0;
      #5 in = 'h76543210; offset = 'h10; arith = 0; direction = 0;
      #5 in = 'h76543210; offset = 'h4; arith = 1; direction = 0;
      #5 in = 'h76543210; offset = 'h8; arith = 1; direction = 0;
      #5 in = 'h76543210; offset = 'hC; arith = 1; direction = 0;
      #5 in = 'h76543210; offset = 'h10; arith = 1; direction = 0;
      #5 in = 'h76543210; offset = 'h4; arith = 0; direction = 1;
      #5 in = 'h76543210; offset = 'h8; arith = 0; direction = 1;
      #5 in = 'h76543210; offset = 'hC; arith = 0; direction = 1;
      #5 in = 'h76543210; offset = 'h10; arith = 0; direction = 1;
      #5 in = 'h76543210; offset = 'h4; arith = 1; direction = 1;
      #5 in = 'h76543210; offset = 'h8; arith = 1; direction = 1;
      #5 in = 'h76543210; offset = 'hC; arith = 1; direction = 1;
      #5 in = 'h76543210; offset = 'h10; arith = 1; direction = 1;
      #5 in = 'h01234567; offset = 'h4; arith = 0; direction = 0;
      #5 in = 'h01234567; offset = 'h0; arith = 0; direction = 0;
      #5 in = 'h01234567; offset = 'h8; arith = 0; direction = 1;
      #5 in = 'h01234567; offset = 'h14; arith = 0; direction = 1;
      #5 in = 'hFFEEDDCC; offset = 'h4; arith = 0; direction = 0;
      #5 in = 'hFFEEDDCC; offset = 'h8; arith = 0; direction = 1;
      #5 in = 'hFFEEDDCC; offset = 'h4; arith = 1; direction = 0;
      #5 in = 'hFFEEDDCC; offset = 'h11; arith = 1; direction = 1;
      #5 in = 'h01234567; offset = 'h1; arith = 1; direction = 1;
  end

endmodule //1bitalutest
