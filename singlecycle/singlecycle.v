module singlecycle(
  input clk, pcreset, 
  output [0:31] ao, bo, aluouto, memrego, instrego
  );

  //control flags
  wire branch;
  wire [0:31] a, b, aluout, memreg, instreg; //multistage checks
  wire [0:31] regwdata; //from MEMTOREG to REGS
  wire [0:31] pc_val, pc_next, pc_in; //from/to IF
  wire [0:4] rs1, rs2, rd; //decoder to regfile
  wire [0:31] ra, rb; //For JAL muxing, to ALU
  wire [0:15] imm16;
  wire [0:2] aluop, setop;
  wire [0:25] name;
  wire [0:31] imm;
  wire imminstr, sign, setinstr, shiftdir, shiftarith, jumpinstr, shiftinstr, rinstr, branchinstr, branchop; //decoder to ALU
  wire regwrt, memwrt, memtreg; //decoder to regfile
  wire zf;  //from ALU
  wire multinstr; //Multiply instruction
  wire [0:1] pc_src, pc_sel; //PC selector
  wire [0:1] dsize; //Data Memory Size
  wire [0:31] dontcare; //for miscellaneous don't care values

  //Code Start
  //Branch Instructions
  assign branch =  ((zf ^ branchop) & branchinstr); //Assign Branch bit
  mux21 BDET0(.in0(pc_src[0]), .in1(1), .sel(branch), .out(pc_sel[0])),
        BDET1(.in0(pc_src[1]), .in1(0), .sel(branch), .out(pc_sel[1]));
  //Main Modules      
  instrfetch INSTRFETCH(.clk(clk), .PC_reset(~pcreset), .PC_src(pc_sel), .imm32(pc_in), .rs(a), .instru(instreg), .PC_val(pc_val), .PC_next(pc_next)); //Instruction Fetch
  fa32 IFB(.a(pc_next), .b(imm), .sum(pc_in), .cout(dontcare)); //Decoder
  topalu ALU(.ra(ra),.rb(rb),.imm(imm), .setinstr(setinstr), .imminstr(imminstr), .shiftinstr(shiftinstr), .aluop(aluop), .setop(setop), .shiftarith(shiftarith), .shiftdir(shiftdir), .out(aluout), .zf(zf)); //Top ALU
  regfile REGS(.clk(~clk), .reset(pcreset), .write(regwrt), .wAddr(rd), .wData(regwdata), .rAddr1(rs1), .rData1(a),.rAddr2(rs2), .rData2(b)); //Register File
  decoder DECODER(.instru(instreg), 
    .rs1(rs1), .rs2(rs2), .rd(rd), .imm16(imm16), .aluop(aluop), .name(name), 
    .imminstr(imminstr), .sign(sign), .jumpinstr(jumpinstr), .rinstr(rinstr), 
    .branchinstr(branchinstr), .branchop(branchop), .regwrt(regwrt), .memwrt(memwrt), 
    .memtreg(memtreg), .dsize(dsize), .setinstr(setinstr), .setop(setop), 
    .shiftinstr(shiftinstr), .shiftdir(shiftdir), .shiftarith(shiftarith), 
    .multinstr(multinstr), .PC_src(pc_src)); //Decoder
  dmem DMEM(.addr(aluout), .rData(memreg), .wData(b), .writeEnable(memwrt), .dsize(dsize), .clk(clk)); //Data Memory
  extender EXTEND(.imm16(imm16), .name(name), .imminstr(imminstr), .immext(sign), .ext_val(imm)); //Sign Extender
  
 
  genvar j;
  wire [0:31] eight;
  assign eight = 8;
  generate
    for(j=0;j<32;j=j+1) begin : memttreg //Memory to Register
      mux21 MEMTOREG(.in0(aluout[j]), .in1(memreg[j]), .sel(memtreg), .out(regwdata[j]));
      mux21 JALRA(a[j], pc_val[j], regwrt & jumpinstr, ra[j]);
      mux21 JALRB(b[j], eight[j], regwrt & jumpinstr, rb[j]);
    end
  endgenerate

  //assign outputs
  assign ao = a;
  assign bo = b;
  assign aluouto = aluout;
  assign memrego = memreg;
  assign instrego = instreg;

endmodule 
