/*Single Cycle Testbench
Stephen Zeng*/
module singlecycle_tb;
  parameter IMEMFILE = "instr.hex";
  parameter DMEMFILE = "data.hex"; 
  reg [8*80-1:0] filename;
  reg clk, reset, tick;
  wire [0:31] ao, bo, aluouto, memrego, instrego;

  singlecycle SINGLECYCLE(.clk(clk), .pcreset(reset), .ao(ao), .bo(bo), .aluouto(aluouto), .memrego(memrego), .instrego(instrego));

  integer i;

  always
  #1 clk = !clk;
  initial begin
	  // Clear DMEM
	  for (i = 0; i < SINGLECYCLE.DMEM.SIZE; i = i+1)
	        SINGLECYCLE.DMEM.mem[i] = 8'h0;
	  //Load Instruction memory Hexfile
	  if (!$value$plusargs("instrfile=%s", filename)) begin
	            filename = IMEMFILE;
	  end
	  $readmemh(filename, SINGLECYCLE.INSTRFETCH.Instrmem.mem);
	  // Load DMEM from file
	  if (!$value$plusargs("datafile=%s", filename)) begin
		    filename = DMEMFILE;
	  end
	  $readmemh(filename, SINGLECYCLE.DMEM.mem);
	  //Monitor Outputs
	  $monitor("ALURES = %h \n Opcode = %h \n IS = %b \n Time = %d \n PC Value = %h \n PC Select = %h \n Instruction = %h \n RS1 = %d \n RS2 = %d \n RD = %d \n IMM16 = %h \n ALUOP = %b \n Name = %h \n Imminstr = %b \n Sign = %b \n Jumpinstr = %b \n Rinstr = %b \n Branchinstr = %b \n Branchop = %b \n Regwrite = %b \n Memwrt = %b \n Mem2reg = %b \n Dsize = %h \n Set = %b \n Setop = %b \n Shiftinstr = %b \n Shiftdir = %b \n Shiftartith= %b \n Extended Imm = %h \n Multinstr = %h \n A: %h \n B: %h \n OUT: %h \n zf: %b \n regwdata: %h\n ra/rb: %h/%h\n\n", 
    SINGLECYCLE.ALU.alures, SINGLECYCLE.DECODER.opcode, SINGLECYCLE.DECODER.is, $time, SINGLECYCLE.pc_val, SINGLECYCLE.pc_src, SINGLECYCLE.INSTRFETCH.instru, 
    SINGLECYCLE.rs1, SINGLECYCLE.rs2, SINGLECYCLE.rd, SINGLECYCLE.imm16, SINGLECYCLE.aluop, SINGLECYCLE.name, SINGLECYCLE.imminstr, SINGLECYCLE.sign, SINGLECYCLE.jumpinstr, SINGLECYCLE.rinstr, SINGLECYCLE.branchinstr, SINGLECYCLE.branchop, 
    SINGLECYCLE.regwrt, SINGLECYCLE.memwrt, SINGLECYCLE.memtreg, SINGLECYCLE.dsize, SINGLECYCLE.setinstr, SINGLECYCLE.setop, SINGLECYCLE.shiftinstr, SINGLECYCLE.shiftdir, SINGLECYCLE.shiftarith, 
  SINGLECYCLE.imm, SINGLECYCLE.multinstr, SINGLECYCLE.a, SINGLECYCLE.b, SINGLECYCLE.aluout, SINGLECYCLE.zf, SINGLECYCLE.regwdata, SINGLECYCLE.ra, SINGLECYCLE.rb);
    clk = 0; reset = 0;
    #2 reset = 1;
  end
  initial 
    #40 $finish;
    
endmodule
