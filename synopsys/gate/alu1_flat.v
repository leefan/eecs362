
module alu1 ( a, b, binv, cin, op, cout, res );
  input [0:1] op;
  input a, b, binv, cin;
  output cout, res;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, n9;

  OAI222_X2 U1 ( .A1(op[0]), .A2(n5), .B1(n6), .B2(n3), .C1(n1), .C2(n4), .ZN(
        res) );
  AOI22_X2 U2 ( .A1(n7), .A2(cin), .B1(n8), .B2(n2), .ZN(n6) );
  NAND2_X2 U5 ( .A1(a), .A2(n9), .ZN(n5) );
  XOR2_X2 U6 ( .A(n9), .B(a), .Z(n8) );
  XOR2_X2 U7 ( .A(binv), .B(b), .Z(n9) );
  INV_X4 U8 ( .A(n8), .ZN(n1) );
  INV_X4 U9 ( .A(cin), .ZN(n2) );
  INV_X4 U10 ( .A(op[0]), .ZN(n3) );
  INV_X4 U11 ( .A(op[1]), .ZN(n4) );
  NOR2_X2 U12 ( .A1(op[1]), .A2(n8), .ZN(n7) );
  OAI21_X2 U13 ( .B1(n1), .B2(n2), .A(n5), .ZN(cout) );
endmodule

