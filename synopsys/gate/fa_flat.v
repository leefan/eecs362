
module fa ( a, b, cin, sum, cout );
  input a, b, cin;
  output sum, cout;
  wire   n2, n3;

  XOR2_X2 U1 ( .A(cin), .B(n2), .Z(sum) );
  AOI22_X2 U2 ( .A1(b), .A2(a), .B1(n2), .B2(cin), .ZN(n3) );
  XOR2_X2 U3 ( .A(a), .B(b), .Z(n2) );
  INV_X4 U4 ( .A(n3), .ZN(cout) );
endmodule

