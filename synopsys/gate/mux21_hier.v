
module mux21 ( in0, in1, sel, out );
  input in0, in1, sel;
  output out;
  wire   n2, n3;

  AOI22_X2 U1 ( .A1(in0), .A2(n2), .B1(sel), .B2(in1), .ZN(n3) );
  INV_X4 U2 ( .A(n3), .ZN(out) );
  INV_X4 U3 ( .A(sel), .ZN(n2) );
endmodule

