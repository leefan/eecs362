
module mux41 ( in0, in1, in2, in3, sel, out );
  input [0:1] sel;
  input in0, in1, in2, in3;
  output out;
  wire   n1, n2, n3, n4;

  OAI22_X2 U1 ( .A1(n3), .A2(n2), .B1(sel[1]), .B2(n4), .ZN(out) );
  AOI22_X2 U2 ( .A1(in0), .A2(n1), .B1(sel[0]), .B2(in2), .ZN(n4) );
  AOI22_X2 U3 ( .A1(in1), .A2(n1), .B1(in3), .B2(sel[0]), .ZN(n3) );
  INV_X4 U4 ( .A(sel[0]), .ZN(n1) );
  INV_X4 U5 ( .A(sel[1]), .ZN(n2) );
endmodule

