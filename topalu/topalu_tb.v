//`timescale 1ns/1np
module topalu_tb;
  //Instruction Fetch
  parameter IMEMFILE = "instr.hex"; 
  reg [8*80-1:0] filename;
  reg clk, reset; //Inputs
  reg PC_sel;
  reg wrt;
  wire [0:31] instru;
  wire [0:31] PC_next;
  //Decoder
  wire [0:4] rsa, rs2, rd;
  wire [0:15] imm16;
  wire [0:2] aluop;
  wire [0:25] name;
  wire imminstr;
  wire sign;
  wire jumpinstr, rinstr;
  wire branchinstr, branchop;
  wire regwrt, memwrt, memtreg;
  wire [0:1] dsize;
  wire set;
  wire [0:2] setop;
  wire shiftinstr, shiftdir, shiftarith;
  wire mul;
  //Extender
  wire [0:31] imm;
  //Top ALU
  reg [0:31] ra, rb; //Inputs
  wire [0:31] out;
  wire zf;
  instrfetch INSTRFETCH(.clk(clk), .PC_reset(reset), .PC_src(PC_sel), .wrt(wrt), .branch_in(imm), .instru(instru), .PC_next(PC_next));
  decoder DECODER(.instru(instru), .rs1(rsa), .rs2(rs2), .rd(rd), .imm16(imm16), .aluop(aluop), .name(name), .imminstr(imminstr), .sign(sign), .jumpinstr(jumpinstr), .rinstr(rinstr), .branchinstr(branchinstr), .branchop(branchop), .regwrt(regwrt), .memwrt(memwrt), .memtreg(memtreg),.dsize(dsize), .setinstr(set), .setop(setop), .shiftinstr(shiftinstr), .shiftdir(shiftdir), .shiftarith(shiftarith), .multinstr(mult));
  extender EXTENDER(.imm16(imm16), .name(name), .imminstr(imminstr), .immext(sign), .ext_val(imm));
  topalu ALU(.ra(ra), .rb(rb), .imm(imm), .setinstr(set), .imminstr(imminstr), .shiftinstr(shiftinstr), .aluop(aluop), .setop(setop), .shiftarith(shiftarith), .shiftdir(shiftdir), .out(out), .zf(zf));
  //Set Clock
  always
  #5 clk = !clk;
  initial begin
  //Load Instruction memory Hexfile
  if (!$value$plusargs("instrfile=%s", filename)) begin
            filename = IMEMFILE;
  end
  $readmemh(filename, INSTRFETCH.Instrmem.mem);
  //Monitor Outputs
  $monitor("ALURES = %h \n Opcode = %h \n IS = %b \n Time = %d \n PC Value = %h \n PC Select = %h \n Instruction = %h \n RS1 = %d \n RS2 = %d \n RD = %d \n IMM16 = %h \n ALUOP = %b \n Name = %h \n Imminstr = %b \n Sign = %b \n Jumpinstr = %b \n Rinstr = %b \n Branchinstr = %b \n Branchop = %b \n Regwrite = %b \n Memwrt = %b \n Mem2reg = %b \n Dsize = %h \n Set = %b \n Setop = %b \n Shiftinstr = %b \n Shiftdir = %b \n Shiftartith= %b \n Extended Imm = %h \n Multinstr = %h \n A: %h \n B: %h \n OUT: %h \n zf: %b \n \n", 
    ALU.alures, DECODER.opcode, DECODER.is, $time, PC_next, PC_sel, INSTRFETCH.instru, 
    rsa, rs2, rd, imm16, aluop, name, imminstr, sign, jumpinstr, rinstr, branchinstr, branchop, regwrt, memwrt, memtreg, dsize, set, setop, shiftinstr, shiftdir, shiftarith, 
  imm, mult, ra, rb, out, zf);
    #0 clk = 0; reset = 1; ra = 0; rb = 0; wrt = 1; PC_sel = 0;
    #5 reset = 0;
    #10 reset = 1;
    #90 reset = 0;
  end
  initial 
    #100 $finish;
    
endmodule
